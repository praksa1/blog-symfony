<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\BlogPost;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $author = new Author();
            $author->setName('author '.$i);
            $author->setTitle('title'.$i);
            $author->setUsername('username'.$i);
            $author->setCompany('company'.$i);
            $author->setShortBio('shortBio'.$i);
            $author->setPhone(mt_rand(61000000, 69000000));
            $author->setFacebook('fb'.$i);
            $author->setTwitter('tw'.$i);
            $author->setGithub('gh'.$i);
            $manager->persist($author);
        }

        $manager->flush();

        for ($i = 0; $i < 20; $i++) {
            $blogPost = new BlogPost();
            $blogPost->setTitle('title '.$i);
            $blogPost->setSlug('slug '.$i);
            $blogPost->setDescription('desc '.$i);
            $blogPost->setBody('body '.$i);
            $blogPost->setAuthor($author);
            
            $manager->persist($blogPost);
        }

        $manager->flush();
    }
}